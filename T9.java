import java.util.Scanner;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Locale;
import java.util.HashMap;

class T9{
    private HashMap<String,Double> dicoFreq;
    /**
      * @param mot
      * une chaine de caractère
      * @return une chaine de caractères correspondant à la suite de touches à taper pour obtenir le mot en entrée.
      *  Par exemple pour le mot 'toto', la fonction renvoie '8686'
      */
    public static String toTouches(String mot){
        // TODO Q3 : à concevoir
        return "8686";
    }
    /**
     * Constructeur
     */
    T9()
    {
        // TODO Q4 : initialiser et remplir dicoFreq
        try{
            Scanner in = new Scanner(new FileReader("liste_mots.txt")).useLocale(Locale.US);
            while(in.hasNext()){
                String s=in.next();
                double d=in.nextDouble();
                dicoFreq.put(s,d);
            }
        }
        catch(FileNotFoundException e){
            System.out.println("Pas de fichier");
        }
    }
    /**
      * @param prefixe
      *     une chaine de caractères correspondant à une suite de touches tapées, par exemple "8686"
      * @return le mot le plus probable correspondant à la suite de touches.
      */
    public String getMot(String prefixe){
        // TODO : à concevoir
        return prefixe;
    }
    /**
      * @param numeroTouche
      *      un entier correspondant à la position de la touche sur le clavier (par exemple la touche '2' est à la position 1.)
      * @return le symbole qui doit être affiché sur la touche. Par exemple, sur l'entrée 1, le symbole est '2', sur l'entrée 9, le symbole est '*'
    */
    public String getSymbole(int numeroTouche){
        // TODO Q1.2 et Q1.3
        if(numeroTouche == 0){
            return "1";
        }
        if(numeroTouche == 1){
            return "2";
        }
        if(numeroTouche == 2){
            return "3";
        }
        if(numeroTouche == 3){
            return "4";
        }
        if(numeroTouche == 4){
            return "5";
        }
        if(numeroTouche == 5){
            return "6";
        }
        if(numeroTouche == 6){
            return "7";
        }
        if(numeroTouche == 7){
            return "8";
        }
        if(numeroTouche == 8){
            return "9";
        }
        if(numeroTouche == 9){
            return "*";
        }
        if(numeroTouche == 10){
            return "0";
        }
        if(numeroTouche == 11){
            return "#";
        }
        return "";
    }

    /**
      * @param numeroTouche
      *      un entier correspondant à la position de la touche sur le clavier (par exemple la touche '2' est à la position 1.)
      * @return les lettres qui correspondent à la touche. Par exemple, sur l'entrée 1, la fonction renvoie "abc" qui correpond au texte de la touche 2.
    */

    public String getTexte(int numeroTouche){
        if(numeroTouche == 0){
            return " ";
        }
        if(numeroTouche == 1){
            return "abc";
        }
        if(numeroTouche == 2){
            return "def";
        }
        if(numeroTouche == 3){
            return "ghi";
        }
        if(numeroTouche == 4){
            return "jkl";
        }
        if(numeroTouche == 5){
            return "mno";
        }
        if(numeroTouche == 6){
            return "pqrs";
        }
        if(numeroTouche == 7){
            return "tuv";
        }
        if(numeroTouche == 8){
            return "wxyz";
        }
        if(numeroTouche == 9){
            return "*";
        }
        if(numeroTouche == 10){
            return "+";
        }
        return "";
    }




}
